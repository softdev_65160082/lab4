/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

/**
 *
 * @author Tauru
 */
public class Table {

    private String[][] table = {{"7", "8", "9"}, {"4", "5", "6"}, {"1", "2", "3"}};
    private Player player1;
    private Player player2;
    private Player playerTurn;
    private int turnCount = 0;
    private String n;

    public Table(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.playerTurn = player1;
        this.turnCount=0;
    }
    public int getTurnCount() {
        return turnCount;
    }

    public String[][] getTable() {
        return table;
    }

    public boolean setInput(String n) {
        this.n = n;
            while (true) {
            boolean checkInput = false;
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    if (n.equals(table[i][j])) {
                        checkInput = true;
                        table[i][j] = playerTurn.getSymbol();
                        turnCount++;
                        break;
                    }
                }
            }
            if (checkInput) {
                break;
            }
            if (!checkInput) {
                continue;
            }
            break;
        }
        return false;
    }

    public boolean checkWin() {
    for (int i = 0; i < 3; i++) {
        if (table[i][0].equals(table[i][1]) && table[i][0].equals(table[i][2])) {
            return true;
        }
    }

    for (int j = 0; j < 3; j++) {
        if (table[0][j].equals(table[1][j]) && table[0][j].equals(table[2][j])) {
            return true;
        }
    }

    if ((table[0][0].equals(table[1][1]) && table[0][0].equals(table[2][2]))
            || (table[0][2].equals(table[1][1]) && table[0][2].equals(table[2][0]))) {
        return true;
    }
    
    return false;
}


    Player getplayerTurn() {
        return playerTurn;

    }

    void switchPlayer() {
        if (playerTurn.equals(player1)) {
            playerTurn = player2;
        } else {
            playerTurn = player1;
        }
    }

    public boolean checkDraw() {
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            if (!table[i][j].equals("X") && !table[i][j].equals("O")) {
                return false;
            }
        }
    }
    return true;
}

}
