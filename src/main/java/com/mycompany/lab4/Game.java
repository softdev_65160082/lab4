/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

import java.util.Scanner;

/**
 *
 * @author Tauru
 */
public class Game {

    private Table table;
    private Player player1, player2;

    public Game() {
        player1 = new Player("X");
        player2 = new Player("O");
    }

    public void Play() {
        showWelcome();
        newGame();
        while (true) {
            showTable();
            showTurn();
            inputNumber();
            if (table.checkWin()) {
                saveStat();
                showTable();
                System.out.println("Turn Count: " + table.getTurnCount());
                System.out.println(table.getplayerTurn().getSymbol() + " wins!");
                newGame();
                inputContinue();
            }
            if (table.checkDraw()) {
                saveDraw();
                showTable();
                System.out.println("Turn Count: " + table.getTurnCount());
                System.out.println("Draw!");
                newGame();
                inputContinue();
            }
            table.switchPlayer();
        }

    }

    public void saveStat() {
        if (table.checkWin()) {
            if (table.getplayerTurn().equals(player1)) {
                player1.win();
                player2.lose();
            } else {
                player1.lose();
                player2.win();
            }
        } else if (table.checkDraw()) {
            player1.draw();
            player2.draw();
        }
    }

    private void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    private void showTable() {
        String[][] t = table.getTable();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(t[i][j] + " ");
            }
            System.out.println();
        }
    }

    private void newGame() {
        table = new Table(player1, player2);
    }

    private void inputNumber() {
    Scanner ip = new Scanner(System.in);
    System.out.print("Please Enter Number : ");
    String n = ip.next();
    table.setInput(n);
}

    private void showTurn() {
        System.out.println(table.getplayerTurn().getSymbol() + " turn");
    }

    private void saveDraw() {
        player1.draw();
        player2.draw();
    }

    public void inputContinue() {
    Scanner ip = new Scanner(System.in);
    while (true) {
        System.out.print("Do you want to continue (yes or no): ");
        String ans = ip.next();
        if (ans.equalsIgnoreCase("yes")) {
            newGame();
            break;
        } else if (ans.equalsIgnoreCase("no")) {
            System.out.println("Thank you for playing.");
            showScores();
            System.exit(0);
        } else {
            System.out.println("Invalid input. Please enter 'yes' or 'no'.");
        }
    }
}
    
    private void showScores() {
    System.out.println("----- Scores -----");
    System.out.println("Player 1 - Wins: " + player1.getWinCount() + ", Losses: " + player1.getLoseCount() + ", Draws: " + player1.getDrawCount());
    System.out.println("Player 2 - Wins: " + player2.getWinCount() + ", Losses: " + player2.getLoseCount() + ", Draws: " + player2.getDrawCount());
}

}
